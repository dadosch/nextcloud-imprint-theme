<?php
/**
 * @author Björn Schießle <schiessle@owncloud.com>
 * @author Jan-Christoph Borchardt, http://jancborchardt.net
 * @author dadosch: Imprint text
 * @copyright Copyright (c) 2016, ownCloud, Inc.
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License, version 3,
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

class OC_Theme {

        /**
         * Returns the base URL
         * @return string URL
         */
        public function getBaseUrl() {
                return 'https://cloud.example.de';
        }

        /**
         * Returns the URL where the sync clients are listed
         * @return string URL
         */
        public function getSyncClientUrl() {
                return 'https://nextcloud.com/install/#install-clients';
        }

        /**
         * Returns the URL to the App Store for the iOS Client
         * @return string URL
         */
        public function getiOSClientUrl() {
                return 'https://itunes.apple.com/us/app/nextcloud/id1125420102?mt=8';
        }

        /**
         * Returns the AppId for the App Store for the iOS Client
         * @return string AppId
         */
        public function getiTunesAppId() {
                return '1125420102';
        }

        /**
         * Returns the URL to Google Play for the Android Client
         * @return string URL
         */
        public function getAndroidClientUrl() {
                return 'https://play.google.com/store/apps/details?id=com.nextcloud.client';
        }

        /**
         * Returns the documentation URL
         * @return string URL
         */
        public function getDocBaseUrl() {
                return 'https://docs.nextcloud.com';
        }

        /**
         * Returns the title
         * @return string title
         */
        public function getTitle() {
                return 'Your Cloud';
        }

        /**
         * Returns the short name of the software
         * @return string title
         */
        public function getName() {
                return 'Your Cloud';
        }

        /**
         * Returns the short name of the software containing HTML strings
         * @return string title
         */
        public function getHTMLName() {
                return 'Your Cloud';
        }

        /**
         * Returns entity (e.g. company name) - used for footer, copyright
         * @return string entity name
        public function getEntity() {
                return '';
        }

        /**
         * Returns slogan
         * @return string slogan
         */
        public function getSlogan() {
                return 'Your custom cloud, personalized for you!';
        }

        /**
         * Returns logo claim
         * @return string logo claim
         * @deprecated 13.0.0 not used anymore
/
        public function getLogoClaim() {
                return '';
        }
*/
        /**
         * Returns short version of the footer
         * @return string short footer
         */
        public function getShortFooter() {
                $footer = '<a href="/legal_de">Impressum &middot; Datenschutzerkärung &middot; Cookie-Hinweis</a>';
                return $footer;
        }

        /**
         * Returns long version of the footer
         * @return string long footer
         */
        public function getLongFooter() {
                $footer = '<a href="/legal_de">Impressum &middot; Datenschutzerklärung &middot; Cookie-Hinweis</a>';
                return $footer;
        }


        public function getColorPrimary() {
                return '#745bca';
        }

        /**
         * Returns variables to overload defaults from core/css/variables.scss
         * @return array

        public function getScssVariables() {
                return [
                        'color-primary' => '#745bca'
                ];
        }
*/
}
